import Fire from '../../assets/Fire.png'
import Pizza from '../../assets/Pizza.png'
import Sushi from '../../assets/Sushi.png'
import Drink from '../../assets/Drink.png'
import Snacks from '../../assets/Snacks.png'
import Combo from '../../assets/Combo.png'
import Dessert from '../../assets/Dessert.png'
import Sauce from '../../assets/Sauce.png'

import Img1 from '../../assets/Group81.png'
import Img2 from '../../assets/Group82.png'
import './showcase.css'


const Showcase = () => {
  return (
    <div>
      <div className='box-all'>
          <div className="box">
              <img src={Fire} alt="" />
              <p>Акции</p>
          </div>
          <div className="box">
              <img src={Pizza} alt="" />
              <p>Пицца</p>
          </div>
          <div className="box">
              <img src={Sushi} alt="" />
              <p>Суши</p>
          </div>
          <div className="box">
              <img src={Drink} alt="" />
              <p>Напитки</p>
          </div>
          <div className="box">
              <img src={Snacks} alt="" />
              <p>Закуски</p>
          </div>
          <div className="box">
              <img src={Combo} alt="" />
              <p>Комбо</p>
          </div>
          <div className="box">
              <img src={Dessert} alt="" />
              <p>Десерты</p>
          </div>
          <div className="box">
              <img src={Sauce} alt="" />
              <p>Соусы</p>
          </div>
      </div>
      <div className='imgall'>
        <div className='box_img'>
          <img src={Img1} alt="" />
        </div>
        <div className='box_img'>
          <img src={Img2} alt="" />
        </div>
        <div className='box_img'>
          <img src={Img1} alt="" />
        </div>
        <div className='box_img'>
          <img src={Img2} alt="" />
        </div>
      </div>
    </div>
  )
}

export default Showcase