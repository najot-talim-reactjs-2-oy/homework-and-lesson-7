import React, { useContext } from 'react'
import { ProductContext } from '../context/ProductContext'
import { Alert, Col, Row } from 'react-bootstrap';
import CardCard from '../components/Card2/CardCard';


const CartPage = () => {
  const { cartProducts } = useContext(ProductContext);
  return (
    <section className='orqafon'>
        <Row>
          {cartProducts.length !== 0 ? cartProducts.map((pr) => (
            <Col className="ro mb-3" key={pr.id} lg={3} md={4} sm={6}>
              <CardCard {...pr} />
            </Col>
          )) : <Alert className='aler-warning'>Free</Alert>}
        </Row>
    </section>
  )
}

export default CartPage